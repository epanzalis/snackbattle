import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const CountdownBackground = styled.div`
  width: 50%;
  background: #FAFAFA;
`;

const CountdownContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background: #f5da7b;
  color: #FFFFFF;
  font-size: 40px;
  font-wight: bold;
`;


class Countdown extends React.Component {
  constructor(props) {
    super(props);

    this.initTemporizer = this.initTemporizer.bind(this);
    this.updateCountDown = this.updateCountDown.bind(this);

    const timer = props.timer ? props.timer : 3;

    this.state = {
      timer,
    }
  }

  componentDidMount() {
    this.initTemporizer();
  }

  componentDidUpdate() {
    const timer = this.state.timer;
    if (timer > 0) {
      this.initTemporizer();
    } else {
      this.props.timeElapsed();
    }
  }

  initTemporizer() {
    this.timeoutID = setTimeout(() => {
      this.updateCountDown();
    }, 1000);
  }

  updateCountDown() {
    const timer = this.state.timer;
    if (timer > 0) {
      this.setState({
        timer: timer - 1,
      });
    }
  }

  render() {
    const timer = this.state.timer;
    const title = this.props.title;

    return (
      <CountdownBackground>
        <CountdownContainer>
          <p>
            { title }
          </p>
          { timer }
        </CountdownContainer>
      </CountdownBackground>
    )
  }
}

Countdown.propTypes = {
  timeElapsed: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  timer: PropTypes.number,
}

export default Countdown
