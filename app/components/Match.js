import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Snack from './Snack';

const MatchContainer = styled.div`
  display: flex;
  justify-content: space-around;
`;

class Match extends React.Component {
  render() {
    const disputerCodes = ['disputer1', 'disputer2'];
    const snacksComponents = disputerCodes.map((disputerCode, index) => {
      return (
        <Snack
          key={index.toString()}
          disputer={this.props.data[disputerCode]}
          disputerCode={disputerCode}
          placeWinner={this.props.placeWinner}
          resetCountdown={this.resetCountdown}
        />
      )
    });

    return (
      <MatchContainer>
        { snacksComponents }
      </MatchContainer>
    );
  }
}

Match.propTypes = {
  data: PropTypes.object.isRequired,
  placeWinner: PropTypes.func.isRequired,
};

export default Match
