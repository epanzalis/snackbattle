import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const SnackName = styled.h2`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 0;
  text-transform: capitalize;
`;

const SnackTitle = styled.h3`
  font-size: 0.8rem;
`;

const SnackImage = styled.img`
  width: 40%;
`;

const SnackDescription = styled.div`
  font-size: 14px;
  text-align: center;
`;

const SnackCointainer = styled.div`
  width: 80%;
  background: #ffedae;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 2rem;
  cursor: pointer;
  border: 1px solid #f5da7b;
  &:hover {
    background: #f5da7b;
  }
`;

const SnackMainCointainer = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
`;

const SnackContainer = (attrs) => {
  return (
    <SnackCointainer>
      <div tabIndex={0} role="button" onClick={attrs.placeWinner}>
        <SnackName>{attrs.name}</SnackName>
        <SnackImage alt={attrs.name} src={attrs.img} />
        <SnackDescription>
          <SnackTitle>{attrs.title}</SnackTitle>
          {attrs.description}
        </SnackDescription>
      </div>
    </SnackCointainer>
  )
}

class Snack extends React.Component {
  constructor() {
    super();

    this.placeWinner = this.placeWinner.bind(this);
  }

  placeWinner() {
    const disputerCode = this.props.disputerCode;
    this.props.placeWinner(disputerCode);
  }

  render() {
    const disputer = this.props.disputer;

    return (
      <SnackMainCointainer>
        <SnackContainer
          placeWinner={this.placeWinner}
          {...disputer}
        />
      </SnackMainCointainer>
    )
  }
}

Snack.propTypes = {
  disputer: PropTypes.object.isRequired,
  placeWinner: PropTypes.func.isRequired,
  disputerCode: PropTypes.string.isRequired,
};

export default Snack
