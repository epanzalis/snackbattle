import React from 'react';
import styled from 'styled-components';
import SnackArray from '../data/snacks_info.json'
import Match from './Match';
import Countdown from './Countdown';

const GameContainer = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

class Game extends React.Component {
  constructor() {
    super();
    this.createMatches = this.createMatches.bind(this);
    this.pickNextMatch = this.pickNextMatch.bind(this);
    this.shuffleArray = this.shuffleArray.bind(this);
    this.placeWinner = this.placeWinner.bind(this);
    this.initSubmatches = this.initSubmatches.bind(this);
    this.switchOffCountdown = this.switchOffCountdown.bind(this);
    this.renderWinner = this.renderWinner.bind(this);

    const snacks = this.shuffleArray(SnackArray['snacks']);
    this.state = {
      matches: this.createMatches(snacks),
      countdownOn: true,
      roundCounter: 1,
      title: 'Get ready!',
    }
  }

  componentDidUpdate() {
    const matches = this.state.matches;

    if (matches.length > 1) {
      const nextMatch = this.pickNextMatch();

      if (!nextMatch) {
        this.initSubmatches();
      }
    }
  }

  getCurrentMatchIndex() {
    return this.state.matches.findIndex((match) => {
      return match.winnerId === null;
    });
  }

  pickNextMatch() {
    const nextMatch = this.state.matches.find((match) => {
      return match.winnerId === null;
    });

    if (nextMatch) {
      this.title = 'Next match...';
    }

    return nextMatch || null;
  }

  initSubmatches() {
    const completedMatches = this.state.matches;
    const roundCounter = this.state.roundCounter;
    const nextRound = completedMatches.map((match) => {
      return match[match.winnerId];
    });

    this.setState({
      matches: this.createMatches(nextRound),
      countdownOn: true,
      roundCounter: roundCounter + 1,
      title: `Round #${roundCounter + 1}...`,
    })
  }

  createMatches(snacks) {
    const couples = [];
    let tempCouple = {};

    snacks.forEach((snack, index) => {
      if (index < 16) {
        if (index % 2 !== 0) {
          tempCouple['disputer2'] = snack;
          couples.push(Object.assign({}, tempCouple));
          tempCouple = {};
        } else {
          tempCouple['disputer1'] = snack;
          tempCouple['winnerId'] = null;
        }
      }
    });

    return couples;
  }

  shuffleArray(array) {
    const arr = array;
    let currentIndex = arr.length;
    let temporaryValue;
    let randomIndex;

    while (currentIndex !== 0) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      temporaryValue = arr[currentIndex];
      arr[currentIndex] = arr[randomIndex];
      arr[randomIndex] = temporaryValue;
    }

    return arr;
  }

  switchOffCountdown() {
    this.setState({
      countdownOn: false,
    })
  }

  placeWinner(disputer) {
    const matches = this.state.matches;
    const currentMatchIndex = this.getCurrentMatchIndex();
    const currentMatch = matches[currentMatchIndex];
    currentMatch.winnerId = disputer;
    this.setState({
      matches,
      countdownOn: true,
      title: 'Next match...',
    });
  }

  renderWinner() {
    const matches = this.state.matches;
    const winner = matches[0][matches[0].winnerId];

    return (
      <div>
        And the winner iiiiis...
        {winner.name}
      </div>
    )
  }

  render() {
    // const matches = this.state.matches;
    const countdownOn = this.state.countdownOn;
    const title = this.state.title;
    const nextMatch = this.pickNextMatch();
    let toRender = '';

    // let toRender = (matches.length === 1 && matches[0].winnerId) ? this.renderWinner() : (
    //   <GameContainer>
    //     <Match data={} placeWinner={this.placeWinner} />
    //   </GameContainer>
    // )


    if (countdownOn) {
      toRender = (
        <Countdown timeElapsed={this.switchOffCountdown} title={title} timer={1} />
      )
    } else if (nextMatch) {
      toRender = (
        <GameContainer>
          <Match data={nextMatch} placeWinner={this.placeWinner} />
        </GameContainer>
      )
    }

    return (
      <div>
        { toRender }
      </div>
    );
  }
}

export default Game
